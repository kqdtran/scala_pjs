package list

abstract class MyAList {
  def head: Any
  def tail: MyAList
  def isEmpty : Boolean
  def length: Int
  def add(item: Any): MyAList = new MyList(item, this)
}

class MyList(val head: Any, val tail: MyAList) extends MyAList {
  def isEmpty = false
  def length: Int = 1 + tail.length
  override def toString: String = head + " " + tail
}

object MyListNil extends MyAList {
  def head: Any = throw new Exception("head of empty list")
  def tail: MyAList = throw new Exception("tail of empty list")
  def isEmpty = true
  def length = 0
  override def toString = ""
}

object MyList extends App {
  override def main(args: Array[String]) {
    var list = MyListNil add("ABC") add("XYZ") add("123")
    println("List: " + list)
    println("Size: " + list.length)
  }
}
