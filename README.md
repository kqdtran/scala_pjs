These includes:

* Fish and Shark: Original spec based on [CS61B's Project 1](http://www.cs.berkeley.edu/~jrs/61bs13/hw/pj1/readme). Kinda like Feeding Frenzy, except that you can't control any creature. Look into SimText.java for directions on how to simulate a text-based game. Run the unit test by 

        scalac *.scala && javac *.java
        scala Test 

* A basic linked-list implementation in Scala. 

More forthcoming (or at least that's the plan)