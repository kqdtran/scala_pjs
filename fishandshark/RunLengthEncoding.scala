/* RunLengthEncoding.scala */
import scala.util.control.Breaks._

/**
 *  The RunLengthEncoding class defines an object that run-length encodes an
 *  Ocean object.  Descriptions of the methods you must implement appear below.
 *  They include constructors of the form
 *
 *      public RunLengthEncoding(int i, int j, int starveTime)
 *      public RunLengthEncoding(int i, int j, int starveTime,
 *                               int[] runTypes, int[] runLengths) {
 *      public RunLengthEncoding(Ocean ocean) {
 *
 *  that create a run-length encoding of an Ocean having width i and height j,
 *  in which sharks starve after starveTime timesteps.
 *
 *  The first constructor creates a run-length encoding of an Ocean in which
 *  every cell is empty.  The second constructor creates a run-length encoding
 *  for which the runs are provided as parameters.  The third constructor
 *  converts an Ocean object into a run-length encoding of that object.
 *
 *  See the README file accompanying this project for additional details.
 */

class RunLengthEncoding(i: Int, j: Int, starveTime: Int) {

    /**
     *  RunLengthEncoding() (with three parameters) is a constructor that creates
     *  a run-length encoding of an empty ocean having width i and height j,
     *  in which sharks starve after starveTime timesteps.
     *  @param i is the width of the ocean.
     *  @param j is the height of the ocean.
     *  @param starveTime is the number of timesteps sharks survive without food.
     */
    private var rle: DList = new DList(i, j, starveTime)
    rle.insertFront(Ocean.EMPTY, i*j, 0)
    private var runsLeft: Int = 1

    /**
     *  RunLengthEncoding() (with five parameters) is a constructor that creates
     *  a run-length encoding of an ocean having width i and height j, in which
     *  sharks starve after starveTime timesteps.  The runs of the run-length
     *  encoding are taken from two input arrays.  Run i has length runLengths[i]
     *  and species runTypes[i].
     *  @param i is the width of the ocean.
     *  @param j is the height of the ocean.
     *  @param starveTime is the number of timesteps sharks survive without food.
     *  @param runTypes is an array that represents the species represented by
     *         each run.  Each element of runTypes is Ocean.EMPTY, Ocean.FISH,
     *         or Ocean.SHARK.  Any run of sharks is treated as a run of newborn
     *         sharks (which are equivalent to sharks that have just eaten).
     *  @param runLengths is an array that represents the length of each run.
     *         The sum of all elements of the runLengths array should be i * j.
     */

    def this(i: Int, j: Int, starveTime: Int, runTypes: Array[Int], runLengths: Array[Int]) = {
        this(i, j, starveTime)
        if (runTypes.length != runLengths.length) {
            println("Incompatible arrays' lengths!")
            System.exit(1)
        } else {
            rle = new DList(i, j, starveTime)
            for (i <- 0 to (runLengths.length - 1)) {
                if (runTypes(i) == Ocean.SHARK) {
                    rle.insertEnd(runTypes(i), runLengths(i), starveTime)
                } else {
                    rle.insertEnd(runTypes(i), runLengths(i), 0)
                }
            }
            runsLeft = runLengths.length
        }
    }

    /**
     *  restartRuns() and nextRun() are two methods that work together to return
     *  all the runs in the run-length encoding, one by one.  Each time
     *  nextRun() is invoked, it returns a different run (represented as an
     *  array of two ints), until every run has been returned.  The first time
     *  nextRun() is invoked, it returns the first run in the encoding, which
     *  contains cell (0, 0).  After every run has been returned, nextRun()
     *  returns null, which lets the calling program know that there are no more
     *  runs in the encoding.
     *
     *  The restartRuns() method resets the enumeration, so that nextRun() will
     *  once again enumerate all the runs as if nextRun() were being invoked for
     *  the first time.
     *
     *  (Note:  Don't worry about what might happen if nextRun() is interleaved
     *  with addFish() or addShark() it won't happen.)
     */

    /**
     *  restartRuns() resets the enumeration as described above, so that
     *  nextRun() will enumerate all the runs from the beginning.
     */

    def restartRuns = runsLeft = rle.length

    /**
     *  nextRun() returns the next run in the enumeration, as described above.
     *  If the runs have been exhausted, it returns null.  The return value is
     *  an array of two ints (constructed here), representing the type and the
     *  size of the run, in that order.
     *  @return the next run in the enumeration, represented by an array of
     *          two ints.  The int at index zero indicates the run type
     *          (Ocean.EMPTY, Ocean.SHARK, or Ocean.FISH).  The int at index one
     *          indicates the run length (which must be at least 1).
     */

    def nextRun: Array[Int] = {
        if (runsLeft == 0) null
        else {
            val nextIndex: Int = rle.length - runsLeft + 1
            val nextNode: DListNode = rle.nth(nextIndex)
            var next: Array[Int] = Array.ofDim[Int](2)
            next(0) = nextNode.creature
            next(1) = nextNode.consecs
            runsLeft -= 1
            next
        }
    }
    
    /**
     *  toOcean() converts a run-length encoding of an ocean into an Ocean
     *  object.  You will need to implement the three-parameter addShark method
     *  in the Ocean class for this method's use.
     *  @return the Ocean represented by a run-length encoding.
     */

    def toOcean: Ocean = {
        var rleOcean: Ocean = new Ocean(rle.x, rle.y, rle.starveTime)
        var xCurr: Int = 0
        var yCurr: Int = 0
        for (pos <- 1 to rle.length) {
            val currNode: DListNode = rle.nth(pos)
            val creature: Int = currNode.creature
            val consecs: Int = currNode.consecs
            val hunger: Int = currNode.hunger

            // Loop thru the RLE, adding stuff to the ocean accordingly
            var c: Int = consecs
            while (c > 0) {
                if (creature == Ocean.SHARK) {
                    rleOcean.addShark(xCurr, yCurr, hunger)
                } 
                if (creature == Ocean.FISH) {
                    rleOcean.addFish(xCurr, yCurr)
                }  
                xCurr += 1
                if (xCurr >= rle.x) {
                    yCurr += 1 // wrap to a new row
                    xCurr = 0
                }
                c -= 1
            }
        }
        rleOcean
    }

    /**
     *  Part III.
     */

    /**
     *  RunLengthEncoding() (with one parameter) is a constructor that creates
     *  a run-length encoding of an input Ocean.  You will need to implement
     *  the sharkFeeding method in the Ocean class for this constructor's use.
     *  @param sea is the ocean to encode.
     */
    def this(sea: Ocean) = {
        this(sea.width, sea.height, sea.starveTime)
        rle = new DList(sea.width, sea.height, sea.starveTime)
        var currentType: Int = sea.cellContents(0, 0)
        var currentHunger: Int = sea.sharkFeeding(0, 0)
        var currentLength: Int = 0
        for (y <- 0 to (sea.height - 1)) {
            for (x <- 0 to (sea.width - 1)) { 
                if (sea.cellContents(x, y) == currentType && sea.sharkFeeding(x, y) == currentHunger) {
                    currentLength += 1
                } else {
                    rle.insertEnd(currentType, currentLength, currentHunger)
                    currentType = sea.cellContents(x, y)
                    currentHunger = sea.sharkFeeding(x, y)
                    currentLength = 1
                }
            }
        }
        rle.insertEnd(currentType, currentLength, currentHunger) // insert the last run
        check
        runsLeft = rle.length
    }    

    /**
     *  Part IV.
     */

    /**
     *  addFish() places a fish in cell (x, y) if the cell is empty.  If the
     *  cell is already occupied, leave the cell as it is.  The final run-length
     *  encoding should be compressed as much as possible there should not be
     *  two consecutive runs of sharks with the same degree of hunger.
     *  @param x is the x-coordinate of the cell to place a fish in.
     *  @param y is the y-coordinate of the cell to place a fish in.
     */

    def addFish(xc: Int, yc: Int) {
        val x: Int = RunLengthEncoding.modulo(xc, rle.x)
        val y: Int = RunLengthEncoding.modulo(yc, rle.y)
        
        // rlePos is the last index of the node right before the removed node in rle
        // i is the index of the EMPTY node to be taken out temporarily
        // index is the position of (x, y) if Ocean is a 1D array, counting from 1
        var rlePos: Int = 1 
        var index: Int = y * rle.x + x + 1 
        var i: Int = 1  

        breakable {
            while (i < rle.length) { // find the correct node
                rlePos += rle.nth(i).consecs
                if (rlePos > index) { 
                    rlePos -= (rle.nth(i).consecs + 1)
                    break
                }
                i += 1
            }
        }

        // Different cases for the inserted FISH
        if (rle.nth(i).creature != Ocean.EMPTY) {
            println("Destination cell is not empty!")
        } else { 
            val removedNode: DListNode = rle.nth(i)
            val runLength: Int = removedNode.consecs
            val spaceAfter: Int = removedNode.consecs - (index - rlePos) 
            val spaceBefore: Int = removedNode.consecs - spaceAfter - 1
            val newNode: DListNode = new DListNode(Ocean.FISH, 1)
            var beforeNode: DListNode = new DListNode()
            var afterNode: DListNode = new DListNode()

            // Run of 1, so we may end up connecting runs of FISH together
            if (runLength == 1) {
                // This cell is sandwiched between two blocks, neither of which is a run of FISH
                if (removedNode.prev.creature != Ocean.FISH && removedNode.next.creature != Ocean.FISH) { 
                    newNode.next = removedNode.next
                    newNode.next.prev = newNode
                    newNode.prev = removedNode.prev
                    newNode.prev.next = newNode
                }

                // This cell is sandwiched between two blocks, both of which are runs of FISH
                else if (removedNode.prev.creature == Ocean.FISH && 
                        removedNode.next.creature == Ocean.FISH) {
                    val combinedRun: Int = removedNode.next.consecs + removedNode.prev.consecs + 1
                    val combinedNode: DListNode = new DListNode(Ocean.FISH, combinedRun)
                    combinedNode.next = removedNode.next.next
                    combinedNode.next.prev = combinedNode
                    combinedNode.prev = removedNode.prev.prev
                    combinedNode.prev.next = combinedNode
                    rle.changeSize(rle.length - 2)
                    runsLeft = rle.length
                }

                // This cell is preceded by a run of FISH
                else if (removedNode.prev.creature == Ocean.FISH) {
                    removedNode.prev.consecs += 1
                    removedNode.prev.next = removedNode.next
                    removedNode.next.prev = removedNode.prev
                    rle.changeSize(rle.length - 1)
                    runsLeft = rle.length
                }

                // This cell is followed by a run of FISH
                else {
                    removedNode.next.consecs += 1
                    removedNode.prev.next = removedNode.next
                    removedNode.next.prev = removedNode.prev
                    rle.changeSize(rle.length - 1)
                    runsLeft = rle.length
                }
            }

            // runLength is greater than 1
            else {
                // if the fish is on the right's boundary, check right neighbor's for same fish
                if (spaceAfter == 0 && removedNode.next.creature == Ocean.FISH) { 
                    removedNode.next.consecs += 1
                    removedNode.consecs -= 1
                }

                // if the fish is on the left's boundary, check left neighbor's for same fish
                else if (spaceBefore == 0 && removedNode.prev.creature == Ocean.FISH) { 
                    removedNode.prev.consecs += 1
                    removedNode.consecs -= 1
                }

                // if the fish is stuck between one (or two) empty spaces, break apart the nodes and reconnect
                else {
                    if (spaceBefore > 0) { // there are still some empty spaces before this fish
                        beforeNode = new DListNode(Ocean.EMPTY, spaceBefore)
                        beforeNode.prev = removedNode.prev
                        removedNode.prev.next = beforeNode
                        beforeNode.next = newNode
                        newNode.prev = beforeNode
                        if (spaceAfter == 0) {
                            newNode.next = removedNode.next
                            newNode.next.prev = newNode
                        }
                        rle.changeSize(rle.length + 1)
                        runsLeft = rle.length
                    }
                    if (spaceAfter > 0) { // there are still some empty spaces after this fish
                        afterNode = new DListNode(Ocean.EMPTY, spaceAfter)
                        afterNode.next = removedNode.next
                        afterNode.next.prev = afterNode
                        afterNode.prev = newNode
                        newNode.next = afterNode
                        if (spaceBefore == 0) {
                            newNode.prev = removedNode.prev
                            newNode.prev.next = newNode
                        }
                        rle.changeSize(rle.length + 1)
                        runsLeft = rle.length
                    }
                }
            }
        }
        check
    } 

    /**
     *  addShark() (with two parameters) places a newborn shark in cell (x, y) if
     *  the cell is empty.  A "newborn" shark is equivalent to a shark that has
     *  just eaten.  If the cell is already occupied, leave the cell as it is.
     *  The final run-length encoding should be compressed as much as possible
     *  there should not be two consecutive runs of sharks with the same degree
     *  of hunger.
     *  @param x is the x-coordinate of the cell to place a shark in.
     *  @param y is the y-coordinate of the cell to place a shark in.
     */
    def addShark(xc: Int, yc: Int) {
        val x: Int = RunLengthEncoding.modulo(xc, rle.x)
        val y: Int = RunLengthEncoding.modulo(yc, rle.y)
        
        // rlePos is the first index of the current node in rle with respect to the ocean + 1
        // i is the index of the EMPTY node to be taken out temporarily
        // index is the position of (x, y) if Ocean is a 1D-array 
        var rlePos: Int = 1 
        var index: Int = y * rle.x + x + 1 
        var i: Int = 1

        breakable {
            while (i < rle.length) { // find the correct node
                rlePos += rle.nth(i).consecs
                if (rlePos > index) { 
                    rlePos -= (rle.nth(i).consecs + 1)
                    break
                }
                i += 1
            }
        }

        // Different cases for the inserted SHARK
        if (rle.nth(i).creature != Ocean.EMPTY) {
            println("Destination cell is not empty!")
        } else { 
            val removedNode: DListNode = rle.nth(i)
            val runLength: Int = removedNode.consecs
            val starveTime: Int = rle.starveTime
            val spaceAfter: Int = removedNode.consecs - (index - rlePos) 
            val spaceBefore: Int = removedNode.consecs - spaceAfter - 1
            val newNode: DListNode = new DListNode(Ocean.SHARK, 1, starveTime)
            var beforeNode: DListNode = new DListNode()
            var afterNode: DListNode = new DListNode()

            // Run of 1, so we may end up connecting runs of SHARKS together
            if (runLength == 1) {
                // This cell is sandwiched between two blocks, neither of which 
                // is a run of SHARK with similar hunger values
                if (removedNode.prev.hunger != starveTime && removedNode.next.hunger != starveTime) { 
                    newNode.next = removedNode.next
                    newNode.next.prev = newNode
                    newNode.prev = removedNode.prev
                    newNode.prev.next = newNode
                }

                // This cell is sandwiched between two blocks, both of which are
                // runs of SHARK with similar hunger values
                else if (removedNode.prev.creature == Ocean.SHARK && 
                        removedNode.next.creature == Ocean.SHARK && 
                        removedNode.prev.hunger == starveTime && 
                        removedNode.next.hunger == starveTime) {
                    val combinedRun: Int = removedNode.next.consecs + removedNode.prev.consecs + 1
                    val combinedNode: DListNode = new DListNode(Ocean.SHARK, combinedRun, starveTime)
                    combinedNode.next = removedNode.next.next
                    combinedNode.next.prev = combinedNode
                    combinedNode.prev = removedNode.prev.prev
                    combinedNode.prev.next = combinedNode
                    rle.changeSize(rle.length - 2)
                    runsLeft = rle.length
                }

                // This cell is preceded by a run of SHARK with similar hunger value
                else if (removedNode.prev.creature == Ocean.SHARK && removedNode.prev.hunger == starveTime) {
                    removedNode.prev.consecs += 1
                    removedNode.prev.next = removedNode.next
                    removedNode.next.prev = removedNode.prev
                    rle.changeSize(rle.length - 1)
                    runsLeft = rle.length
                }

                // This cell is followed by a run of SHARK with similar hunger value
                else {
                    removedNode.next.consecs += 1
                    removedNode.prev.next = removedNode.next
                    removedNode.next.prev = removedNode.prev
                    rle.changeSize(rle.length - 1)
                    runsLeft = rle.length
                } 
            }

            // runLength is greater than 1
            else {
                // if the shark is on the right's boundary, check right neighbor's for same shark
                if (spaceAfter == 0 && removedNode.next.hunger == starveTime) { 
                    removedNode.next.consecs += 1
                    removedNode.consecs -= 1
                }

                // if the shark is on the left's boundary, check left neighbor's for same shark
                else if (spaceBefore == 0 && removedNode.prev.hunger == starveTime) { 
                    removedNode.prev.consecs += 1
                    removedNode.consecs -= 1
                }

                // if shark is in the middle, break it into 3 parts: space, shark, space
                else {
                    if (spaceBefore > 0) { // there are still some empty spaces before this shark
                        beforeNode = new DListNode(Ocean.EMPTY, spaceBefore)
                        beforeNode.prev = removedNode.prev
                        removedNode.prev.next = beforeNode
                        beforeNode.next = newNode
                        newNode.prev = beforeNode
                        if (spaceAfter == 0) {
                            newNode.next = removedNode.next
                            newNode.next.prev = newNode
                        }
                        rle.changeSize(rle.length + 1)
                        runsLeft = rle.length
                    }
                    if (spaceAfter > 0) { // there are still some empty spaces after this shark
                        afterNode = new DListNode(Ocean.EMPTY, spaceAfter)
                        afterNode.next = removedNode.next
                        afterNode.next.prev = afterNode
                        afterNode.prev = newNode
                        newNode.next = afterNode
                        if (spaceBefore == 0) {
                            newNode.prev = removedNode.prev
                            newNode.prev.next = newNode
                        }
                        rle.changeSize(rle.length + 1)
                        runsLeft = rle.length
                    }
                }
            }               
        }
        check
    }

    /**
     *  check() walks through the run-length encoding and prints an error message
     *  if two consecutive runs have the same contents, or if the sum of all run
     *  lengths does not equal the number of cells in the ocean.
     */

    private def check {
        if (rle.length < 1) { // Rule 3
            println("RLE object's length cannot be less than 1!")
        } else {
            var sumLength: Int = 0
            for (pos <- 1 to (rle.length - 1)) {
                // same type in consecutive runs, and same hunger values 
                // (trivial for EMPTY and FISH since it's 0). Rule 1
                if (rle.nth(pos).creature == rle.nth(pos + 1).creature &&
                    rle.nth(pos).hunger == rle.nth(pos + 1).hunger) {
                    println("Same type of cells in two consecutive RLE nodes!"
                            + "\nProblem is at runs " + pos + " and " + (pos+1))
                }
                sumLength += rle.nth(pos).consecs
            }
            sumLength += rle.nth(rle.length).consecs // add in last node's length
            
            // Check for Rule 2
            if (sumLength != rle.x * rle.y) {
                println("Sum of all runs doesn't equal the ocean's size! RLE's length is " +
                        sumLength + ", while the ocean's size is " + rle.x * rle.y)
            }
        }
    }
}

object RunLengthEncoding {
    /**
     *  Take the modulo of num from modulus
     *  @param num - the number to be performed the modulo operation on
     *  @param modulus - the modulus (i.e. the number to be wrapped around)
     *  @return the number in the range [0,...,modulus-1]
     */
    private def modulo(num: Int, modulus: Int): Int = {
        var r: Int = num % modulus
        if (r < 0) {
            r += modulus
        }
        r
    }
}
