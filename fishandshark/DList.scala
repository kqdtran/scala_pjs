
class DList {

    /**
     *  The DList class is a doubly-linked implementation of the linked list
     *  abstraction.  DLists are mutable data structures, which can grow at either end.
     */

    private val head: DListNode = new DListNode() 
    head.next = head
    head.prev = head
    private var size: Int = 0
    var x: Int = 0
    var y: Int = 0
    var starveTime: Int = 0 

    /**
     * Auxiliary DList() constructor for Ocean
     */
	
    def this(i: Int, j: Int, starve: Int) =  {
        this()
        x = i
        y = j
        starveTime = starve
    }
    
    /**
     *  isEmpty() indicates whether the list is empty.
     *  @return true if the list is empty, false otherwise.
     **/
    def isEmpty: Boolean = size == 0

    /**
     *  Changes the size of this list, only on valid arguments 
     **/
    def changeSize(newSize: Int): Unit = {
        if (newSize >= size - 2 && newSize <= size + 2) size = newSize
    }
    
    /**
     *  length() returns the length of this list.
     *  @return the length of this list.
     **/
    def length: Int = size

    /**
     *  insertFront() inserts the node at the beginning of this list.
     **/
    def insertFront(creature: Int, consecs: Int, hunger: Int): Unit = {
        var newNode: DListNode = new DListNode(creature, consecs, hunger)
        newNode.next = head.next
        head.next.prev = newNode
        newNode.prev = head
        head.next = newNode
        size += 1
    }

    /**
     *  insertEnd() inserts the node at the end of this list.
     **/
    def insertEnd(creature: Int, consecs: Int, hunger: Int): Unit = {
        var newNode: DListNode = new DListNode(creature, consecs, hunger)
        head.prev.next = newNode
        newNode.next = head
        newNode.prev = head.prev
        head.prev = newNode
        size += 1
    }

    /**
     *  nth() returns the node at the specified position.  If position < 1 or
     *  position > this.length(), null is returned.  Otherwise, the node at
     *  position "position" is returned.  The list does not change.
     *  @param position the desired position, from 1 to length(), in the list.
     *  @return the node at the given position in the list.
     **/
    def nth(position: Int): DListNode = {
        var currentNode: DListNode = null
        var pos = position
        if ((pos < 1) || (head.next == head)) null
        else {
            currentNode = head.next
            while (pos > 1) {
                currentNode = currentNode.next
                if (currentNode == head) null
                else pos -= 1
            }
            currentNode
        }
    }
}
