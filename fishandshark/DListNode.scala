
class DListNode(t: Int, c: Int, h: Int) {

    /**
     *  DListNode is a class used internally by the DList class.  An DList object
     *  is a doubly-linked list, and an DListNode is a node of a doubly-linked
     *  list.  Each DListNode has three references:  one to an object, one to
     *  the previous node in the list, and one to the next node in the list.
     */

    /**
     *  This implementation of DListNode is tailored towards the Ocean game,
     *  which explains the addition of various variables like type, hunger, etc.
     */
    
    var creature: Int = t      // the type of the creature, EMPTY=0, SHARK=1, FISH=2
    var hunger: Int = h    // how many turns it has left (for SHARK only)
    var consecs: Int = c   // how many similar creatures there are (SHARK with 
                           // different hunger values are different)
    var prev: DListNode = null
    var next: DListNode = null

    /**
     *  Auxiliary constructors tailor toward specific purposes
     */ 
    def this(t: Int, c: Int) = this(t, c, 0)
	
    def this(t: Int) = this(t, 0, 0)
    
    def this() = this(0, 0, 0)

    /**
     *  toString for DListNode
     */
    override def toString = "(" + creature + " " + consecs + " " + hunger + ")" 
}
