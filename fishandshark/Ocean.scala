/* Ocean.scala */

/**
 *  The Ocean class defines an object that models an ocean full of sharks and
 *  fish.  Descriptions of the methods you must implement appear below.  They
 *  include a constructor of the form
 *
 *      Ocean(x: Int, y: Int, s: Int);
 *
 *  that creates an empty ocean having width x and height y, in which sharks
 *  starve after starveTime s timesteps.
 *
 *  See the README file accompanying this project for additional details.
 */

class Ocean(x: Int, y: Int, s: Int) {

    require(x > 0 && y > 0 && s >= 0)

    /**
     *  Define any variables associated with an Ocean object here.  These
     *  variables MUST be private.
     */
    private var i: Int = x                 // width - x axis
    private var j: Int = y                 // height - y axis
    private var starve: Int = s 
    private var cells = Array.ofDim[Int](i, j)
    private var hunger = Array.ofDim[Int](i, j)

    /**
     *  Part I.
     */

    /**
     *  width() returns the width of an Ocean object.
     *  @return the width of the ocean.
     */
    def width: Int = i

    /**
     *  height() returns the height of an Ocean object.
     *  @return the height of the ocean.
     */
    def height: Int = j 

    /**
     *  starveTime() returns the number of timesteps sharks survive without food.
     *  @return the number of timesteps sharks survive without food.
     */
    def starveTime: Int = starve
    
    /**
     *  addFish() places a fish in cell (x, y) if the cell is empty.  If the
     *  cell is already occupied, leave the cell as it is.
     *  @param x is the x-coordinate of the cell to place a fish in.
     *  @param y is the y-coordinate of the cell to place a fish in.
     */
    def addFish(xc: Int, yc: Int) {
        val x: Int = Ocean.modulo(xc, i)
        val y: Int = Ocean.modulo(yc, j)
        if (cells(x)(y) == Ocean.EMPTY) {
            cells(x)(y) = Ocean.FISH
        }
    }

    /**
     *  addShark() (with two parameters) places a newborn shark in cell (x, y) if
     *  the cell is empty.  A "newborn" shark is equivalent to a shark that has
     *  just eaten.  If the cell is already occupied, leave the cell as it is.
     *  @param x is the x-coordinate of the cell to place a shark in.
     *  @param y is the y-coordinate of the cell to place a shark in.
     */
    def addShark(xc: Int, yc: Int) {
        val x: Int = Ocean.modulo(xc, i)
        val y: Int = Ocean.modulo(yc, j)
        if (cells(x)(y) == Ocean.EMPTY) {
            cells(x)(y) = Ocean.SHARK
            hunger(x)(y) = starve
        }  
    }

    /**
     *  cellContents() returns EMPTY if cell (x, y) is empty, FISH if it contains
     *  a fish, and SHARK if it contains a shark.
     *  @param x is the x-coordinate of the cell whose contents are queried.
     *  @param y is the y-coordinate of the cell whose contents are queried.
     */
    def cellContents(xc: Int, yc: Int): Int = {
        val x: Int = Ocean.modulo(xc, i)
        val y: Int = Ocean.modulo(yc, j)
        return cells(x)(y)
    }

    /**
     *  Returns the neighbors of the current cell as an array
     *  @param x is the x-coordinate of the cell whose contents are queried.
     *  @param y is the y-coordinate of the cell whose contents are queried.
     */ 
    private def neighborArray(x: Int, y: Int): Array[Int] = {
        var neighbors: Array[Int] = Array.ofDim[Int](8)
        neighbors(0) = cellContents(x-1, y-1)
        neighbors(1) = cellContents(x, y-1)
        neighbors(2) = cellContents(x+1, y-1)
        neighbors(3) = cellContents(x-1, y)
        neighbors(4) = cellContents(x+1, y)
        neighbors(5) = cellContents(x-1, y+1)
        neighbors(6) = cellContents(x, y+1)
        neighbors(7) = cellContents(x+1, y+1)
        neighbors
    }

    /**
     *  Counts the number of neighbors of desired type of the current position
     *  @param x is the x-coordinate of the cell whose contents are queried.
     *  @param y is the y-coordinate of the cell whose contents are queried.
     *  @param type - the type of ocean's element, either EMPTY, FISH, or SHARK
     */
    private def countNeighbors(x: Int, y: Int, creature: Int): Int = {
        var sum: Int = 0
        val neighbors: Array[Int] = neighborArray(x, y)
        for (i <- 0 to (neighbors.length - 1)) {
            if (neighbors(i) == creature) {
                sum += 1
            }
        }
        sum
    }

    /**
     *  Counts the number of shark neighbors
     *  @param x is the x-coordinate of the cell whose contents are queried.
     *  @param y is the y-coordinate of the cell whose contents are queried.
     */
    private def countSharkNeighbors(x: Int, y: Int): Int = {
        countNeighbors(x, y, Ocean.SHARK)
    }

    /**
     *  Counts the number of fish neighbors
     *  @param x is the x-coordinate of the cell whose contents are queried.
     *  @param y is the y-coordinate of the cell whose contents are queried.
     */
    private def countFishNeighbors(x: Int, y: Int): Int = {
        countNeighbors(x, y, Ocean.FISH)
    }

    /**
     *  timeStep() performs a simulation timestep as described in README.
     *  @return an ocean representing the elapse of one timestep.
     */
    def timeStep: Ocean = {
        var next = new Ocean(i, j, starve)
        for (x <- 0 to (i -1 )) {
            for (y <- 0 to (j - 1)) {
                val fishNeighbors:Int = countFishNeighbors(x, y)
                val sharkNeighbors:Int = countSharkNeighbors(x, y)
                
                if (cellContents(x, y) == Ocean.SHARK) { // cell contains a shark
                    if (fishNeighbors >= 1) {
                        next.cells(x)(y) = Ocean.SHARK
                        next.hunger(x)(y) = starve
                    } else {
                        if (hunger(x)(y) == 0) {
                            next.cells(x)(y) = Ocean.EMPTY
                        } else {
                            next.cells(x)(y) = Ocean.SHARK
                            next.hunger(x)(y) = hunger(x)(y) - 1
                        }
                    }
                } else if (cellContents(x, y) == Ocean.FISH) { // cell contains a fish    
                    if (sharkNeighbors == 0) {
                        next.cells(x)(y) = Ocean.FISH
                    } else if (sharkNeighbors == 1) {
                        next.cells(x)(y) = Ocean.EMPTY
                    } else {
                        next.cells(x)(y) = Ocean.SHARK
                        next.hunger(x)(y) = starve
                    }
                } else { // cellContents(x, y) == EMPTY               
                    if (fishNeighbors < 2) {
                        next.cells(x)(y) = Ocean.EMPTY
                    } else if (fishNeighbors >= 2 && sharkNeighbors <= 1) {
                        next.cells(x)(y) = Ocean.FISH
                    } else {
                        next.cells(x)(y) = Ocean.SHARK
                        next.hunger(x)(y) = starve
                    }
                }
            }
        }
        next
    }

    /**
     *  Part II.
     */

    /**
     *  addShark() (with three parameters) places a shark in cell (x, y) if the
     *  cell is empty.  The shark's hunger is represented by the third parameter.
     *  If the cell is already occupied, leave the cell as it is.  You will need
     *  this method to help convert run-length encodings to Oceans.
     *  @param x is the x-coordinate of the cell to place a shark in.
     *  @param y is the y-coordinate of the cell to place a shark in.
     *  @param feeding is an integer that indicates the shark's hunger.  You may
     *         encode it any way you want; for instance, "feeding" may be the
     *         last timestep the shark was fed, or the amount of time that has
     *         passed since the shark was last fed, or the amount of time left
     *         before the shark will starve.  It's up to you, but be consistent.
     */
    def addShark(xc: Int, yc: Int, feeding: Int) {
        val x: Int = Ocean.modulo(xc, i)
        val y: Int = Ocean.modulo(yc, j)
        if (cells(x)(y) == Ocean.EMPTY) {
            cells(x)(y) = Ocean.SHARK
            hunger(x)(y) = feeding
        }  
    }

    /**
     *  Part III.
     */

    /**
     *  sharkFeeding() returns an integer that indicates the hunger of the shark
     *  in cell (x, y), using the same "feeding" representation as the parameter
     *  to addShark() described above.  If cell (x, y) does not contain a shark,
     *  then its return value is undefined--that is, anything you want.
     *  Normally, this method should not be called if cell (x, y) does not
     *  contain a shark.  You will need this method to help convert Oceans to
     *  run-length encodings.
     *  @param x is the x-coordinate of the cell whose contents are queried.
     *  @param y is the y-coordinate of the cell whose contents are queried.
     */

    def sharkFeeding(xc: Int, yc: Int): Int = {
        if (cellContents(xc, yc) != Ocean.SHARK) 0
        else {
            val x: Int = Ocean.modulo(xc, i)
            val y: Int = Ocean.modulo(yc, j)
            return hunger(x)(y)
        }
    }
}

object Ocean {

    /**
     *  Do not rename these constants.  WARNING:  if you change the numbers, you
     *  will need to recompile Test4.java.  Failure to do so will give you a very
     *  hard-to-find bug.
     */
    val EMPTY: Int = 0
    val SHARK: Int = 1
    val FISH: Int = 2

    /**
     *  Take the modulo of num from modulus
     *  @param num - the number to be performed the modulo operation on
     *  @param modulus - the modulus (i.e. the number to be wrapped around)
     *  @return the number in the range [0,...,modulus-1]
     */
    private def modulo(num: Int, modulus: Int): Int = {
        var r: Int = num % modulus
        if (r < 0) {
            r += modulus
        }
        r
    }
}
